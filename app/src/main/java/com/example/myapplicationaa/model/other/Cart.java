package com.example.myapplicationaa.model.other;

public class Cart {
    public int IDProductCart;
    public String nameProductCart;
    public String nameArProductCart;
    public String nameGrProductCart;


    public int totalPrice;
    public String oldPriceCart;
    public String newPriceCart;
    public String imgProductCart;
    public int sizeCart;
    public int bill;
    public Cart() {
    }

    public Cart(String nameProductCart, String totalPrice, String oldPriceCart, String newPriceCart, String imgProductCart, int sizeCart) {
        this.nameProductCart = nameProductCart;
        this.totalPrice = IDProductCart;
        this.oldPriceCart = oldPriceCart;
        this.newPriceCart = newPriceCart;
        this.imgProductCart = imgProductCart;
        this.sizeCart = sizeCart;
    }

    public String getNameProductCart() {
        return nameProductCart;
    }

    public void setNameProductCart(String nameProductCart) {
        this.nameProductCart = nameProductCart;
    }

    public int getTotalPriceProductCart() {
        return totalPrice;
    }

    public void setTotalPriceProductCart(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOldPriceCart() {
        return oldPriceCart;
    }

    public void setOldPriceCart(String oldPriceCart) {
        this.oldPriceCart = oldPriceCart;
    }

    public String getNewPriceCart() {
        return newPriceCart;
    }

    public void setNewPriceCart(String newPriceCart) {
        this.newPriceCart = newPriceCart;
    }

    public String getImgProductCart() {
        return imgProductCart;
    }

    public void setImgProductCart(String imgProductCart) {
        this.imgProductCart = imgProductCart;
    }

    public int getSizeCart() {
        return sizeCart;
    }

    public void setSizeCart(int sizeCart) {
        this.sizeCart = sizeCart;
    }

    public int getIDProductCart() {
        return IDProductCart;
    }
}

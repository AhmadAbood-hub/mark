package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.other.CategoryTab;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.carts.CartFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;

import com.example.myapplicationaa.viewHolder.ViewHolderCategoryTab;

import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

public class CategoriesTabAdapter extends RecyclerView.Adapter<ViewHolderCategoryTab> {
    private Context context;
    private List<CategoryTab> categoryTabs;
    private Fragment fragment;
    private Boolean a = false;
    public SharedPreferences sp;


    public CategoriesTabAdapter(Context context, List<CategoryTab> categoryTabs, Fragment fragment) {
        this.context = context;
        this.categoryTabs = categoryTabs;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public ViewHolderCategoryTab onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.categories_tab_item, parent, false);
        return new ViewHolderCategoryTab(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderCategoryTab viewHolderCategoryTab, final int position) {

        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);

        final CategoryTab categoryTab = categoryTabs.get(position);
        viewHolderCategoryTab.getTxtNameDepartmentTab().setText(categoryTab.getName());
        final boolean click = categoryTab.getClick();

        viewHolderCategoryTab.getView().setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                a = true;
                for (int i = 0; i < categoryTabs.size(); i++) {
                    categoryTabs.get(i).setClick(false);
                }
                viewHolderCategoryTab.getTxtNameDepartmentTab().setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                categoryTab.setClick(!categoryTab.getClick());
                notifyDataSetChanged();

                if (position == 0) {
                    ((CategoriesFragment) fragment).MainTabPressed();
                } else {
                    AllProductsFragment.category = new Categories();
                    int language = sp.getInt("language", 0);

                    if (language == 1) {
                        AllProductsFragment.category.name_ar = (categoryTab.getName());

                    } else if (language == 0) {
                        AllProductsFragment.category.name_en = (categoryTab.getName());
                    }
                    ((CategoriesFragment) fragment).OtherTabPressed(categoryTab.getId());
                }

                CategoriesFragment.pos = position;
                CategoriesFragment.ID = categoryTab.getId();


            }
        });


        viewHolderCategoryTab.getTxtNameDepartmentTab().setBackgroundResource(click ? R.color.colorPrimary : R.color.colorWhite);
        viewHolderCategoryTab.getTxtNameDepartmentTab().setTextColor(click ? ContextCompat.getColor(context, R.color.colorWhite) : ContextCompat.getColor(context, R.color.black));

        if (position == 0 && !a && CategoriesFragment.pos == 0) {
            viewHolderCategoryTab.getTxtNameDepartmentTab().setBackgroundResource(R.color.colorPrimary);
            viewHolderCategoryTab.getTxtNameDepartmentTab().setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
        }

        if (position == CategoriesFragment.pos && CategoriesFragment.pos != 0) {
            viewHolderCategoryTab.getTxtNameDepartmentTab().setBackgroundResource(R.color.colorPrimary);
            viewHolderCategoryTab.getTxtNameDepartmentTab().setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            AllProductsFragment.category = new Categories();
            int language = sp.getInt("language", 0);

            if (language == 1) {
                AllProductsFragment.category.name_ar = (categoryTab.getName());

            } else if (language == 0) {
                AllProductsFragment.category.name_en = (categoryTab.getName());
            }
            ((CategoriesFragment) fragment).OtherTabPressed(CategoriesFragment.ID);
        }

    }

    @Override
    public int getItemCount() {
        return categoryTabs.size();
    }


    public void setData(List<CategoryTab> categoryTabs) {
        this.categoryTabs = categoryTabs;
        notifyDataSetChanged();
    }

}

package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.other.CustomFilter;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.detail_product.DetailsProductFragment;
import com.example.myapplicationaa.viewHolder.ViewHolderAllProduct;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class AllProductAdapter extends RecyclerView.Adapter<ViewHolderAllProduct> implements Filterable {
    private Context context;
    public  List<Product> products, mProductsFilter;
    private Fragment fragment;
    CustomFilter filter;
    public SharedPreferences sp;

    public AllProductAdapter(Context context, List<Product> products, Fragment fragment) {
        this.context = context;
        this.products = products;
        this.fragment=fragment;
        mProductsFilter = products;
    }


    @NonNull
    @Override
    public ViewHolderAllProduct onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_item, parent, false);
        return new ViewHolderAllProduct(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderAllProduct viewHolderAllProduct, final int position) {

        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);


        final Product product = products.get(position);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            viewHolderAllProduct.getTxtProductItemNameProduct().setText(product.getName_ar());
            viewHolderAllProduct.getTxtProductItemDescriptionProduct().setText(product.getDescription_ar());

        } else if (language == 0) {
            viewHolderAllProduct.getTxtProductItemNameProduct().setText(product.getName_en());
            viewHolderAllProduct.getTxtProductItemDescriptionProduct().setText(product.getDescription_en());
        }

        viewHolderAllProduct.getTxtProductItemNewPriceProduct().setText(String.valueOf(product.getPrice_after())+ " €");
        viewHolderAllProduct.getTxtProductItemOldPriceProduct().setText(String.valueOf(product.getPrice_befor())+ " €");


        Picasso.get().load(product.getPicture()).into(viewHolderAllProduct.getImgProductItemImageProduct());

        viewHolderAllProduct.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.AllProductsFragment = false;
                MainActivity.DetailsProductFragment = true;
                DetailsProductFragment.mainProduct = product;
                ((MainActivity) context).changeFragment(new DetailsProductFragment());

            }
        });
    }
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter((ArrayList<Product>) mProductsFilter, this);

        }
        return filter;
    }
    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setData(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }


}

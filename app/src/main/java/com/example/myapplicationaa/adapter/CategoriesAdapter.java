package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.example.myapplicationaa.view.Fragments.categories.CategoriesFragment;
import com.example.myapplicationaa.viewHolder.ViewHolderCategory;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByCategory;
import static com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment.ByOffer;

public class CategoriesAdapter extends RecyclerView.Adapter<ViewHolderCategory> {
    private Context context;
    private List<Categories> categories;
    public SharedPreferences sp;

    public CategoriesAdapter(Context context, List<Categories> categories) {
        this.context = context;
        this.categories = categories;

    }


    @NonNull
    @Override
    public ViewHolderCategory onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.categories_item, parent, false);
        return new ViewHolderCategory(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderCategory viewHolderCategory, final int position) {
        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);

        final Categories category = categories.get(position);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            viewHolderCategory.getTxtDepartment().setText(category.name_ar);


        } else if (language == 0) {
            viewHolderCategory.getTxtDepartment().setText(category.name_en);

        }
        //.placeholder(R.drawable.food)
        Picasso.get().load(category.picture).into(viewHolderCategory.getImgDepartment());


        viewHolderCategory.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.AllProductsFragment = true;
                MainActivity.DetailsProductFragment = false;


                CategoriesFragment.ProductsByCategory = true;
                ByCategory = true;

                CategoriesFragment.ProductsByOffer = false;
                ByOffer = false;

                MainActivity.filter = true;

                AllProductsFragment.category = category;
                CategoriesFragment.goneRecycleTap();
                ((MainActivity) context).changeFragment(new AllProductsFragment());

            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setData(List<Categories> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

}

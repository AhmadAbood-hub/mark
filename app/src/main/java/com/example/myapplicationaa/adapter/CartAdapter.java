package com.example.myapplicationaa.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.other.Cart;
import com.example.myapplicationaa.model.other.DataBase;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Fragments.carts.CartFragment;
import com.example.myapplicationaa.viewHolder.ViewHolderCart;
import com.squareup.picasso.Picasso;
import java.util.List;
import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<ViewHolderCart>
{
    private Context context;
    private List<Cart> carts;
    private Fragment fragment;
    public SharedPreferences sp;

    public CartAdapter(Context context, List<Cart> carts, Fragment fragment) {
        this.context = context;
        this.carts = carts;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public ViewHolderCart onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new ViewHolderCart(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderCart viewHolderCart, final int position) {

        final Cart cart = carts.get(position);


        sp = context.getSharedPreferences("THIS", MODE_PRIVATE);
        int language = sp.getInt("language", 0);




        viewHolderCart.getTxtSizeCart().setText(cart.sizeCart + "");

        Picasso.get().load(cart.imgProductCart).into(viewHolderCart.getImgImageProductCart());

        if (language == 1) {
            viewHolderCart.getTxtNameProductCart().setText(cart.nameArProductCart);
            viewHolderCart.getTxtOld().setText("سعر قديم");
            viewHolderCart.getTxtNew().setText("سعر جديد");
            viewHolderCart.getTxtTotalPriceItemText().setText("السعر الكلي");
        } else if (language == 0) {
            viewHolderCart.getTxtNameProductCart().setText(cart.getNameProductCart());
            viewHolderCart.getTxtOld().setText("Old Price");
            viewHolderCart.getTxtNew().setText("New Price");
            viewHolderCart.getTxtTotalPriceItemText().setText("Total Price");
        }

        viewHolderCart.getTxtOldPriceCart().setText(cart.getOldPriceCart());
        viewHolderCart.getTxtNewPriceCart().setText(cart.getNewPriceCart());
        viewHolderCart.getTxtTotalPriceItem().setText(cart.totalPrice + "");

        ((CartFragment) fragment).updateTotalPrice();
        viewHolderCart.getImgAddToCart().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int sizeCart = Integer.parseInt(DataBase.ExecuteScalar(context, "select sizeProduct from product where IDProduct=" + cart.IDProductCart));
                int nowSizeCart = sizeCart + 1;
                viewHolderCart.getTxtSizeCart().setText(nowSizeCart + "");
                int lastNewPrice = Integer.parseInt(DataBase.ExecuteScalar(context, "select totalPrice from product where IDProduct=" + cart.IDProductCart));
                int NewPrice = Integer.parseInt(cart.newPriceCart);
                int RightPrice = lastNewPrice + NewPrice;
                DataBase.ExecuteNonQuery(context, "update product set sizeProduct=" + nowSizeCart + " where IDProduct=" + cart.IDProductCart);
                DataBase.ExecuteNonQuery(context, "update product set totalPrice=" + RightPrice + " where IDProduct=" + cart.IDProductCart);
                viewHolderCart.getTxtTotalPriceItem().setText(RightPrice + "");
                updateTotalPrice(viewHolderCart);
                ((CartFragment) fragment).updateTotalPrice();
            }
        });

        viewHolderCart.getImgDeleteCart().setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                int sizeCart = Integer.parseInt(DataBase.ExecuteScalar(context, "select sizeProduct from product where IDProduct=" + cart.IDProductCart));
                if (sizeCart > 1) {

                    int nowSizeCart = (sizeCart) - 1;

                    int lastNewPrice = Integer.parseInt(DataBase.ExecuteScalar(context, "select totalPrice from product where IDProduct=" + cart.IDProductCart));
                    int NewPrice = Integer.parseInt(cart.newPriceCart);
                    int RightPrice = lastNewPrice - NewPrice;
                    DataBase.ExecuteNonQuery(context, "update product set sizeProduct=" + nowSizeCart + " where IDProduct=" + cart.IDProductCart);
                    DataBase.ExecuteNonQuery(context, "update product set totalPrice=" + RightPrice + " where IDProduct=" + cart.IDProductCart);
                    viewHolderCart.getTxtTotalPriceItem().setText(RightPrice + "");
                    viewHolderCart.getTxtSizeCart().setText(nowSizeCart + "");
                    updateTotalPrice(viewHolderCart);
                    ((CartFragment) fragment).updateTotalPrice();


                } else {
                    DataBase.ExecuteNonQuery(context, "delete from product where IDProduct = " + cart.getIDProductCart());
                    ((MainActivity) context).changeFragment(new CartFragment());
                }


            }
        });

        StrikeOldPrice(viewHolderCart);




    }

    @Override
    public int getItemCount() {
        return carts.size();
    }


    public void setData(List<Cart> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    private void StrikeOldPrice(ViewHolderCart viewHolderCart) {
        SpannableString ss = new SpannableString(viewHolderCart.getTxtOldPriceCart().getText().toString());
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();

        String aa = viewHolderCart.getTxtOldPriceCart().getText().toString();
        int s = aa.length();
        ss.setSpan(strikethroughSpan, 0, s, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        viewHolderCart.getTxtOldPriceCart().setText(ss);
    }

    public void updateTotalPrice(ViewHolderCart viewHolderCart){
        int X = Integer.parseInt(DataBase.ExecuteScalar(context, "select sum(totalPrice) from product"));
        ((CartFragment) fragment).updateSeekBar(X);
    }


}

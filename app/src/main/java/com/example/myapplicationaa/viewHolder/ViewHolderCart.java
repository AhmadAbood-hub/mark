package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.myapplicationaa.R;

public class ViewHolderCart extends RecyclerView.ViewHolder {


    private View view;

    private TextView txtNameProductCart;
    private TextView txtIDProductCart;
    private TextView txtOldPriceCart;
    private TextView txtNewPriceCart;
    private TextView txtSizeCart;

    private TextView txtOld;
    private TextView txtNew;
    private TextView txtTotalPriceItemText;

    private TextView txtTotalPriceItem;

    private RelativeLayout relOne;
    private RelativeLayout relTwo;

    private ImageView imgImageProductCart;
    private ImageView imgDeleteCart;
    private ImageView imgAddToCart;


    public ViewHolderCart(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        txtNameProductCart = (TextView) view.findViewById(R.id.txtNameProductCart);
        txtIDProductCart = (TextView) view.findViewById(R.id.txtIDProductCart);
        txtOldPriceCart = (TextView) view.findViewById(R.id.txtOldPriceCart);
        txtNewPriceCart = (TextView) view.findViewById(R.id.txtNewPriceCart);
        txtSizeCart = (TextView) view.findViewById(R.id.txtSizeCart);


        txtOld = (TextView) view.findViewById(R.id.txtOld);
        txtNew = (TextView) view.findViewById(R.id.txtNew);
        txtTotalPriceItemText = (TextView) view.findViewById(R.id.txtTotalPriceItemText);


        txtTotalPriceItem = (TextView) view.findViewById(R.id.txtTotalPriceItem);

        relOne = (RelativeLayout) view.findViewById(R.id.relOne);
        relTwo = (RelativeLayout) view.findViewById(R.id.relTwo);

        imgImageProductCart = (ImageView) view.findViewById(R.id.imgImageProductCart);
        imgDeleteCart = (ImageView) view.findViewById(R.id.imgDeleteCart);
        imgAddToCart = (ImageView) view.findViewById(R.id.imgAddToCart);


    }


    public View getView() {
        return view;
    }


    public TextView getTxtNameProductCart() {
        return txtNameProductCart;
    }

    public TextView getTxtIDProductCart() {
        return txtIDProductCart;
    }

    public TextView getTxtOldPriceCart() {
        return txtOldPriceCart;
    }

    public TextView getTxtNewPriceCart() {
        return txtNewPriceCart;
    }

    public TextView getTxtSizeCart() {
        return txtSizeCart;
    }



    public TextView getTxtOld() {
        return txtOld;
    }

    public TextView getTxtNew() {
        return txtNew;
    }

    public TextView getTxtTotalPriceItemText() {
        return txtTotalPriceItemText;
    }



    public TextView getTxtTotalPriceItem() {
        return txtTotalPriceItem;
    }



    public RelativeLayout getRelOne() {
        return relOne;
    }

    public RelativeLayout getRelTwo() {
        return relTwo;
    }


    public ImageView getImgImageProductCart() {
        return imgImageProductCart;
    }

    public ImageView getImgDeleteCart() {
        return imgDeleteCart;
    }

    public ImageView getImgAddToCart() {
        return imgAddToCart;
    }


}

package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationaa.R;

public class ViewHolderOrderDetails extends RecyclerView.ViewHolder {


    private View view;

    private TextView txtNameItemOrder;
    private TextView txtQuantityItemOrder;
    private TextView txtIDItemOrder;

    public ViewHolderOrderDetails(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        txtNameItemOrder = (TextView) view.findViewById(R.id.txtNameItemOrder);
        txtQuantityItemOrder = (TextView) view.findViewById(R.id.txtQuantityItemOrder);
        txtIDItemOrder = (TextView) view.findViewById(R.id.txtIDItemOrder);
    }


    public View getView() {
        return view;
    }


    public TextView getTxtNameItemOrder() {
        return txtNameItemOrder;
    }

    public TextView getTxtQuantityItemOrder() {
        return txtQuantityItemOrder;
    }

    public TextView getTxtIDItemOrder() {
        return txtIDItemOrder;
    }


}

package com.example.myapplicationaa.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationaa.R;


public class ViewHolderMainProduct extends RecyclerView.ViewHolder {


    private View view;

    private CardView main_product_card;
    private TextView main_product_text;
    private ImageView main_product_image;



    public ViewHolderMainProduct(@NonNull View itemView) {
        super(itemView);

        this.view = itemView;

        main_product_card = (CardView) view.findViewById(R.id.main_product_card);
        main_product_text = (TextView) view.findViewById(R.id.main_product_text);
        main_product_image = (ImageView) view.findViewById(R.id.main_product_image);

    }


    public View getView() {
        return view;
    }


    public TextView get_main_product_text() {
        return main_product_text;
    }
    public CardView get_main_product_card() {
        return main_product_card;
    }
    public ImageView get_main_product_image() {
        return main_product_image;
    }


}

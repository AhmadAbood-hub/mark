package com.example.myapplicationaa.view.Fragments.profile;


import com.example.myapplicationaa.model.EditUser;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.User;

import java.util.List;

public interface ProfileView {
    void onShowLoading();
    void onHideLoading();
    void onGetProfileResult(User user);
    void onUpdateProfileResult(EditUser update);
    void onGetProfileError(String message);
}

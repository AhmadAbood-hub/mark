package com.example.myapplicationaa.view.Activites.login;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.User;
import com.example.myapplicationaa.model.UserValidate;
import com.example.myapplicationaa.view.Activites.VerifayEmailActivity;
import com.example.myapplicationaa.view.Activites.create_new_user.CreateNewUserActivity;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import com.facebook.FacebookSdk;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private static final String TAG = "GoogleSignInActivity";
    private static final int RC_SIGN_IN = 9001;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    CallbackManager mCallbackManager;
    private EditText etPhoneNumber;

    private Button googleSignIn;
    private Button facebookSignIn;
    private Button phoneSignIn;
    private Button btnLogin;
    private Button sign_up_button;


    private EditText edtEmail;
    private EditText edtPassword;

    String Email;
    String Password;


    LoginPresenter mPresenter;

    ProgressDialog progressDialog;

    public SharedPreferences sp;
    public SharedPreferences.Editor edit;

    User mUser;

    ScrollView mScrollView;

    CircularProgressBar circularProgressBar;

    private AlertDialog.Builder alertDialog;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        sp = getSharedPreferences("THIS", MODE_PRIVATE);
        edit = sp.edit();

        mScrollView = findViewById(R.id.sslogin);

        circularProgressBar = findViewById(R.id.categories_progress_bar_login);
        circularProgressBar.setVisibility(View.GONE);

        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);

        RelativeLayout myLayout = (RelativeLayout) findViewById(R.id.login_relative);


        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("please wait");

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);

        mPresenter = new LoginPresenter(this);

        sign_up_button = findViewById(R.id.sign_up_button);
        phoneSignIn = findViewById(R.id.btnLoginPhone);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        btnLogin = findViewById(R.id.btnLogin);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Email = edtEmail.getText().toString();
                Password = edtPassword.getText().toString();

                mPresenter.userLogin(Email, Password);
            }
        });

        sign_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, VerifayEmailActivity.class));
            }
        });

        phoneSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = etPhoneNumber.getText().toString();
                if (phoneNumber.isEmpty()) {
                    alertDialog = new AlertDialog.Builder(LoginActivity.this);
                    alertDialog.setMessage("Enter your phone number...");
                    alertDialog.show();
                } else {
                    //verify phone number
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            "+963" + phoneNumber, 60, TimeUnit.SECONDS, LoginActivity.this,
                            new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                                @Override
                                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                                    signInUser(phoneAuthCredential);
                                }

                                @Override
                                public void onVerificationFailed(FirebaseException e) {
                                    Log.d(TAG, "onVerificationFailed:" + e.getLocalizedMessage());
                                }

                                @Override
                                public void onCodeSent(final String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                                    super.onCodeSent(verificationId, forceResendingToken);
                                    //
                                    Dialog dialog = new Dialog(LoginActivity.this);
                                    dialog.setContentView(R.layout.verify_popup);

                                    final EditText etVerifyCode = dialog.findViewById(R.id.etVerifyCode);
                                    Button btnVerifyCode = dialog.findViewById(R.id.btnVerifyOTP);
                                    btnVerifyCode.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String verificationCode = etVerifyCode.getText().toString();
                                            if (verificationId.isEmpty()) return;
                                            //create a credential
                                            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, verificationCode);
                                            signInUser(credential);
                                        }
                                    });

                                    dialog.show();
                                }
                            });
                }
            }
        });


        FacebookSdk.sdkInitialize(LoginActivity.this);

        mAuth = FirebaseAuth.getInstance();

        mCallbackManager = CallbackManager.Factory.create();
        facebookSignIn = findViewById(R.id.login_facebook);

        facebookSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());

                        Log.i(TAG, "onSuccess: " + "aaaa");
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.i(TAG, "onError: " + error.toString());
                    }
                });
            }
        });


        googleSignIn = findViewById(R.id.sign_up_google);
        mAuth = FirebaseAuth.getInstance();

        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        int language = sp.getInt("language", 0);


        if (language == 1) {
            sign_up_button.setText("تسجيل حساب");
            googleSignIn.setText("تسجيل حساب مع كوكل");
            facebookSignIn.setText("تسجيل حساب مع فيسبوك");
            edtEmail.setHint("البريد الالكتروني");
            edtPassword.setHint("كلمة السر");
            btnLogin.setText("تسجيل الدخول");

        } else if (language == 0) {

            sign_up_button.setText("Signup");
            googleSignIn.setText("Signup With Google");
            facebookSignIn.setText("Signup With Facebook");
            edtEmail.setHint("Email");
            edtPassword.setHint("Password");
            btnLogin.setText("Login");


        }


    }


    private void signInUser(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            startActivity(new Intent(LoginActivity.this, CreateNewUserActivity.class));
                            CreateNewUserActivity.email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                        } else {
                            Log.d(TAG, "onComplete:" + task.getException().getLocalizedMessage());
                        }
                    }
                });
    }


    private void signIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: Success");

                            CreateNewUserActivity.email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                            startActivity(new Intent(LoginActivity.this, CreateNewUserActivity.class));

                        } else {
                            Log.d(TAG, "onComplete: Failure: ", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);


            } catch (ApiException e) {
                Log.d(TAG, "onActivityResult: Sign in FAILED: ", e);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            alertDialog = new AlertDialog.Builder(LoginActivity.this);

                            int language = sp.getInt("language", 0);

                            if (language == 1) {
                                alertDialog.setMessage("تم التحقق من الحساب");
                            } else if (language == 0) {
                                alertDialog.setMessage("Authentication successfully...");
                            }
                            alertDialog.show();

                            CreateNewUserActivity.email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                            startActivity(new Intent(LoginActivity.this, CreateNewUserActivity.class));

                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.


                            alertDialog = new AlertDialog.Builder(LoginActivity.this);

                            int language = sp.getInt("language", 0);

                            if (language == 1) {
                                alertDialog.setMessage("فشل التحقق من الحساب");
                            } else if (language == 0) {
                                alertDialog.setMessage("Authentication failed...");
                            }
                            alertDialog.show();

                            //   updateUI(null);
                        }

                        // ...
                    }
                });
    }


    @Override
    public void onShowLoading() {

//        mScrollView.setBackgroundResource(R.color.green_opacity);
        circularProgressBar.setVisibility(View.VISIBLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
//                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
//        mScrollView.setBackgroundResource(R.color.colorWhite);
        circularProgressBar.setVisibility(View.GONE);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGetLoginResult(UserValidate user) {


        if (user != null) {
            if (user.getSuccess() == true) {
                mUser = user.getUser();

                edit.putInt("user_id", mUser.id);
                edit.apply();


                alertDialog = new AlertDialog.Builder(LoginActivity.this);

                int language = sp.getInt("language", 0);

                if (language == 1) {
                    alertDialog.setMessage("حساب صحيح");
                } else if (language == 0) {
                    alertDialog.setMessage("valid User...");
                }
                alertDialog.show();

                startActivity(new Intent(LoginActivity.this, MainActivity.class));

            } else {

                alertDialog = new AlertDialog.Builder(LoginActivity.this);

                int language = sp.getInt("language", 0);

                if (language == 1) {
                    alertDialog.setMessage("حساب خاطئ");
                } else if (language == 0) {
                    alertDialog.setMessage("Invalid User...");
                }
                alertDialog.show();
            }
        }
    }

    @Override
    public void onGetLoginError(String message) {
        //    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}

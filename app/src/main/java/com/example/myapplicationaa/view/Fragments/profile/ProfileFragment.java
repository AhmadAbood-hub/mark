package com.example.myapplicationaa.view.Fragments.profile;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.EditUser;
import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.User;
import com.example.myapplicationaa.view.Activites.MainActivity;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment implements ProfileView {

    public static EditText edtUserNameUpdate;
    public static EditText edtEmailUpdate;
    public static EditText edtPasswordUpdate;
    public static EditText edtFirstNameUpdate;
    public static EditText edtLastNameUpdate;
    public static EditText edtPhoneUpdate;
    public static EditText edtPrefectureUpdate;
    public static EditText edtCountryUpdate;
    public static EditText edtPostcodeUpdate;
    public static EditText edtCityUpdate;
    public static EditText edtNumBuildingUpdate;
    public static EditText edtNumStreetUpdate;


    public static String Name;
    public static String Email;
    public static String Password;
    public static String FirstName;
    public static String LastName;
    public static String Phone;
    public static String Country;
    public static String Postcode;
    public static String Prefecture;
    public static String City;
    public static String NumBuilding;
    public static String NumStreet;

    private TextView EmailInfoUpdate;
    private TextView UserInfoUpdate;
    private TextView LocationInfoUpdate;


    public static ProfilePresenter mPresenter;


    SharedPreferences sp;

    User user;

    int user_id;


    private AlertDialog.Builder alertDialog;


    View view;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);


        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);
        user_id = sp.getInt("user_id", 0);

        edtUserNameUpdate = view.findViewById(R.id.edtUserNameUpdate);
        edtEmailUpdate = view.findViewById(R.id.edtEmailUpdate);
        edtPasswordUpdate = view.findViewById(R.id.edtPasswordUpdate);
        edtFirstNameUpdate = view.findViewById(R.id.edtFirstNameUpdate);
        edtLastNameUpdate = view.findViewById(R.id.edtLastNameUpdate);
        edtPhoneUpdate = view.findViewById(R.id.edtPhoneUpdate);
        edtPrefectureUpdate = view.findViewById(R.id.edtPrefectureUpdate);
        edtCountryUpdate = view.findViewById(R.id.edtCountryUpdate);
        edtPostcodeUpdate = view.findViewById(R.id.edtPostcodeUpdate);
        edtCityUpdate = view.findViewById(R.id.edtCityUpdate);
        edtNumBuildingUpdate = view.findViewById(R.id.edtNumBuildingUpdate);
        edtNumStreetUpdate = view.findViewById(R.id.edtNumStreetUpdate);



        EmailInfoUpdate = view.findViewById(R.id.EmailInfoUpdate);
        UserInfoUpdate = view.findViewById(R.id.UserInfoUpdate);
        LocationInfoUpdate = view.findViewById(R.id.LocationInfoUpdate);

        int language = sp.getInt("language", 0);

        if (language == 1) {
            UserInfoUpdate.setText("معلومات المستخدم");
            LocationInfoUpdate.setText("معلومات الموقع");
            EmailInfoUpdate.setText("معلومات البريد الالكتروني");

            edtUserNameUpdate.setHint("اسم المستخدم");
            edtEmailUpdate.setHint("البريد الالكتروني");
            edtCityUpdate.setHint("المدينة");
            edtCountryUpdate.setHint("الدولة");
            edtFirstNameUpdate.setHint("الأسم الأول");
            edtLastNameUpdate.setHint("الأسم الأخير");
            edtNumBuildingUpdate.setHint("رقم المبنى");
            edtNumStreetUpdate.setHint("رقم الشارع");
            edtPasswordUpdate.setHint("كلمة السر");
            edtPostcodeUpdate.setHint("رمز المدينة");
            edtPrefectureUpdate.setHint("المقاطعة");
            edtPhoneUpdate.setHint("الهاتف");


        } else if (language == 0) {
            UserInfoUpdate.setText("User Info");
            LocationInfoUpdate.setText("Location Info");
            EmailInfoUpdate.setText("Email Info");

            edtUserNameUpdate.setHint("UserName");
            edtEmailUpdate.setHint("Email");
            edtCityUpdate.setHint("City");
            edtCountryUpdate.setHint("Country");
            edtFirstNameUpdate.setHint("FirstName");
            edtLastNameUpdate.setHint("LastName");
            edtNumBuildingUpdate.setHint("NumBuilding");
            edtNumStreetUpdate.setHint("NumStreet");
            edtPasswordUpdate.setHint("Password");
            edtPostcodeUpdate.setHint("Postcode");
            edtPrefectureUpdate.setHint("Prefecture");
            edtPhoneUpdate.setHint("Phone");


        }


        mPresenter = new ProfilePresenter(this);

        mPresenter.getProfileUserByID(user_id);

        return view;
    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }

    @Override
    public void onGetProfileResult(User user) {
        this.user = user;

        edtUserNameUpdate.setText(user.name_en);
        edtEmailUpdate.setText(user.email);
        edtPasswordUpdate.setText(user.password);
        edtFirstNameUpdate.setText(user.first_name_en);
        edtLastNameUpdate.setText(user.last_name_en);
        edtPhoneUpdate.setText(user.phone);
        edtPrefectureUpdate.setText(user.prefecture);
        edtCountryUpdate.setText(user.country);
        edtPostcodeUpdate.setText(user.postcard);
        edtCityUpdate.setText(user.city);
        edtNumBuildingUpdate.setText(user.num_building);
        edtNumStreetUpdate.setText(user.num_street);
    }

    @Override
    public void onUpdateProfileResult(EditUser update) {




        int language = sp.getInt("language", 0);

        if (language == 1) {
            alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("تمت عملية التعديل");
            alertDialog.show();

        } else if (language == 0) {
            alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("the update is done");
            alertDialog.show();

        }





    }

    @Override
    public void onGetProfileError(String message) {



        int language = sp.getInt("language", 0);

        if (language == 1) {
            alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("فشلت عملية التعديل ...");
            alertDialog.show();

        } else if (language == 0) {
            alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("the update is failed ...");
            alertDialog.show();

        }
    }


    @Override
    public void onStart() {
        super.onStart();
        MainActivity.hideBottom();

    }


}

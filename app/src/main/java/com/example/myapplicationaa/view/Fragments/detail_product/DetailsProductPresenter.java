package com.example.myapplicationaa.view.Fragments.detail_product;

import com.example.myapplicationaa.api.ApiClient;
import com.example.myapplicationaa.api.ApiInterface;
import com.example.myapplicationaa.model.Components;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsProductPresenter {

    DetailsProductView view;
    DetailsProductPresenter(DetailsProductView view){this.view=view;}

    public  void getComponentsByProduct(int id){
                view.onShowLoading();

        ApiInterface apiInterface = ApiClient.getApiClient()
                .create(ApiInterface.class);
        Call<List<Components>> call = apiInterface.getComponentsByProduct(id);

        call.enqueue(new Callback<List<Components>>() {
            @Override
            public void onResponse(Call<List<Components>> call, Response<List<Components>> response) {
                view.onHideLoading();

                if (response.isSuccessful() && response.body() != null) {
                    view.onGetComponentsByProductResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Components>> call, Throwable t) {
                view.onHideLoading();
                view.onGetComponentsByProductError(t.getLocalizedMessage());
            }
        });
    }
}

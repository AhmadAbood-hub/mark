package com.example.myapplicationaa.view.Activites.login;

import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.UserValidate;

import java.util.List;

public interface LoginView {
    void onShowLoading();
    void onHideLoading();
    void onGetLoginResult(UserValidate user);
    void onGetLoginError(String message);
}

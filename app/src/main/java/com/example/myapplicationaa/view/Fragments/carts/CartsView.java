package com.example.myapplicationaa.view.Fragments.carts;


import com.example.myapplicationaa.model.Order;
import com.example.myapplicationaa.model.Point;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.other.OrderResponse;

import java.util.List;

public interface CartsView {
    void onShowLoading();
    void onHideLoading();
    void onGetOrdersResult(OrderResponse order);
    void onGetOrdersError(String message);

    void onGetLimitOrderResult(List<Point> order);
    void onGetLimitOrderError(String message);
}

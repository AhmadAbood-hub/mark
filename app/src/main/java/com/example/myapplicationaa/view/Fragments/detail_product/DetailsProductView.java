package com.example.myapplicationaa.view.Fragments.detail_product;


import com.example.myapplicationaa.model.Components;
import com.example.myapplicationaa.model.Product;

import java.util.List;

public interface DetailsProductView {
    void onShowLoading();
    void onHideLoading();
    void onGetComponentsByProductResult(List<Components> components);
    void onGetComponentsByProductError(String message);
}

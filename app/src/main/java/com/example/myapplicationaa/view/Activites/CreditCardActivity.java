package com.example.myapplicationaa.view.Activites;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplicationaa.R;
import com.example.myapplicationaa.model.other.DataBase;
import com.example.myapplicationaa.view.Fragments.carts.CartFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;
import com.stripe.android.view.CardMultilineWidget;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreditCardActivity extends AppCompatActivity {

    private static final String BACKEND_URL = "https://stripe-payment-backend-and.herokuapp.com/";
    private OkHttpClient httpClient = new OkHttpClient();
    private String paymentIntentClientSecret;
    private Stripe stripe;

    public static double amount;

    CardMultilineWidget card_multiline_widget;

    private AlertDialog.Builder alertDialog;

    public SharedPreferences sp;

    int language;

    Button payButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);


        card_multiline_widget = findViewById(R.id.card_multiline_widget);

        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull("pk_test_51HHKiLF52RSFzBKPVKzY7gkN8V6XbIyI3tlGs9diBe0YQhis3HzjzrwNY09AYOMKQxxCk6BqqzAbl4vy0fAdv2JC00sCdYR7aw")
        );
        startCheckout();

        sp = getSharedPreferences("THIS", MODE_PRIVATE);

        language = sp.getInt("language", 0);


        if (language == 1) {
            payButton.setText("دفع");

        } else if (language == 0) {
            payButton.setText("Pay");
        }

    }

    private void startCheckout() {
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");


        Map<String, Object> payMap = new HashMap<>();
        Map<String, Object> itemMap = new HashMap<>();

        List<Map<String, Object>> itemList = new ArrayList<>();
        payMap.put("currency", "usd");
        itemMap.put("id", "photo_subscription");
        itemMap.put("amount", amount * 100);
        itemList.add(itemMap);
        payMap.put("items", itemList);
        String json = new Gson().toJson(payMap);


        RequestBody body = RequestBody.create(mediaType, json);
        Request request = new Request.Builder()
                .url(BACKEND_URL + "create-payment-intent")
                .post(body)
                .build();
        httpClient.newCall(request)
                .enqueue(new PayCallback(this));


        // Hook up the pay button to the card widget and stripe instance
        payButton = findViewById(R.id.payButton);
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardMultilineWidget cardInputWidget = findViewById(R.id.card_multiline_widget);
                PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
                if (params != null) {
                    try {
                        ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                                .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
                        stripe.confirmPayment(CreditCardActivity.this, confirmParams);
                    }
                    catch (Exception e)
                    {

                    }

                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    private final class PayCallback implements Callback {
        @NonNull
        private final WeakReference<CreditCardActivity> activityRef;

        PayCallback(@NonNull CreditCardActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(@NonNull Call call, @NonNull IOException e) {
            final CreditCardActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }
          /*  activity.runOnUiThread(() ->
                    Toast.makeText(
                            activity, "Error: " + e.toString(), Toast.LENGTH_LONG
                    ).show()
            );*/
            if (language == 1) {
                alertDialog.setMessage("هناك خطأ في الدفع...");
            } else if (language == 0) {
                alertDialog.setMessage("there is error in payment ...");
            }
            alertDialog.show();
        }

        @Override
        public void onResponse(@NonNull Call call, @NonNull final Response response)
                throws IOException {
            final CreditCardActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }
            if (!response.isSuccessful()) {

                alertDialog = new AlertDialog.Builder(CreditCardActivity.this);

                int language = sp.getInt("language", 0);

                if (language == 1) {
                    alertDialog.setMessage("هناك خطأ في الدفع...");
                } else if (language == 0) {
                    alertDialog.setMessage("there is error in payment ...");
                }
                alertDialog.show();

            } else {
                activity.onPaymentSuccess(response);
            }
        }
    }

    private void onPaymentSuccess(@NonNull final Response response) throws IOException {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> responseMap = gson.fromJson(
                Objects.requireNonNull(response.body()).string(),
                type
        );
        paymentIntentClientSecret = responseMap.get("clientSecret");
    }

    private final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<CreditCardActivity> activityRef;

        PaymentResultCallback(@NonNull CreditCardActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            final CreditCardActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }
            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();

                int language = sp.getInt("language", 0);

                alertDialog = new AlertDialog.Builder(CreditCardActivity.this);



                if (language == 1) {
                    alertDialog.setMessage("تم الدفع بنجاح...");
                } else if (language == 0) {
                   alertDialog.setMessage("payment is successful ...");
                }
                alertDialog.show();
                CartFragment.mPresenter.addOrders();

                DataBase.ExecuteNonQuery(CreditCardActivity.this, "delete from product");
                CartFragment.all_data.clear();
                CartFragment.cartAdapter.setData(CartFragment.all_data);

                CartFragment.btnConfirmOrder.setVisibility(View.GONE);

                CartFragment.mTxtTextTotlal.setVisibility(View.GONE);

                CartFragment.mTxtTotalPrice.setVisibility(View.GONE);

                CartFragment.recFrgCarts.setVisibility(View.GONE);

                CartFragment.noCart.setVisibility(View.VISIBLE);

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed – allow retrying using a different payment method
                int language = sp.getInt("language", 0);

                if (language == 1) {
                    alertDialog.setMessage("هناك خطأ في الدفع...");
                } else if (language == 0) {
                    alertDialog.setMessage("there is error in payment ...");
                }
                alertDialog.show();
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final CreditCardActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }
            // Payment request failed – allow retrying using the same payment method
            int language = sp.getInt("language", 0);

            if (language == 1) {
                alertDialog.setMessage("هناك خطأ في الدفع...");
            } else if (language == 0) {
                alertDialog.setMessage("there is error in payment ...");
            }
            alertDialog.show();
        }


    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }


}

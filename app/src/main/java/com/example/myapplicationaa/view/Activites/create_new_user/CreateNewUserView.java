package com.example.myapplicationaa.view.Activites.create_new_user;

import com.example.myapplicationaa.model.Register;
import com.example.myapplicationaa.model.UserValidate;

public interface CreateNewUserView {
    void onShowLoading();
    void onHideLoading();
    void onGetCreateUserResult(Register user);
    void onGetCreateUserError(String message);
}

package com.example.myapplicationaa.view.Fragments.categories;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import com.example.myapplicationaa.R;
import com.example.myapplicationaa.adapter.CategoriesAdapter;
import com.example.myapplicationaa.adapter.CategoriesTabAdapter;
import com.example.myapplicationaa.adapter.MainProductAdapter;
import com.example.myapplicationaa.adapter.SliderPagerAdapter;
import com.example.myapplicationaa.model.Categories;
import com.example.myapplicationaa.model.Offers;
import com.example.myapplicationaa.model.Product;
import com.example.myapplicationaa.model.ProductORCategory;
import com.example.myapplicationaa.model.other.CategoryTab;
import com.example.myapplicationaa.view.Activites.MainActivity;
import com.example.myapplicationaa.view.Activites.SettingsActivity;
import com.example.myapplicationaa.view.Fragments.all_products.AllProductsFragment;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;


public class CategoriesFragment extends Fragment implements CategoriesView {


    public static RecyclerView recDepartmentTab;
    private RecyclerView.LayoutManager layoutManagerDepartmentTab;
    private CategoriesTabAdapter categoriesTabAdapter;
    private List<CategoryTab> categoryTabs;

    public static boolean ProductsByCategory = false;

    public static boolean ProductsByOffer = false;

    private ImageView btnArrowRight;
    private ImageView btnArrowLeft;

    private RecyclerView recDepartment;
    private RecyclerView recProduct;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.LayoutManager layoutManagerPro;
    private CategoriesAdapter categoriesAdapter;
    public static MainProductAdapter productAdapter;
    private List<Categories> mCategories;
    public static List<Product> mProducts;


    LinearLayout noWifi;
    LinearLayout noItems;

    Button tryAgain;
    TextView noInternetCategory;


    CategoriesPresenter mPresenter;
    CircularProgressBar circularProgressBar;

    RelativeLayout mLayout;
    private List<Offers> mSlides;

    public ViewPager sliderpager;

    public TextView allDep, allProd;

    public TabLayout indicator;

    private FragmentActivity myContext;

    Timer timer = new Timer();
    Handler updateSlide = new Handler();
    private static int delay = 5000; //delay before moving to the next slide.
    private static int wait = 5000;

    public static int pos = 0;
    public static int ID = 0;

    private static final int REQUEST_CODE = 1234;

    public SharedPreferences sp;

    int language;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        sp = getActivity().getSharedPreferences("THIS", MODE_PRIVATE);

        language = sp.getInt("language", 0);


        mProducts = new ArrayList<>();
        mCategories = new ArrayList<>();

        btnArrowRight = view.findViewById(R.id.btnArrowRight);
        btnArrowLeft = view.findViewById(R.id.btnArrowLeft);


        btnArrowRight.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);

        btnArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sliderpager.getCurrentItem() > 0) {

                    sliderpager.setCurrentItem(sliderpager.getCurrentItem() - 1);

                } else {

                    sliderpager.setCurrentItem(mSlides.size() - 1);
                }
            }
        });


        btnArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sliderpager.getCurrentItem() < mSlides.size() - 1) {

                    sliderpager.setCurrentItem(sliderpager.getCurrentItem() + 1);

                } else {

                    sliderpager.setCurrentItem(0);
                }
            }
        });
        noWifi = view.findViewById(R.id.noWifi);
        noItems = view.findViewById(R.id.noItems);


        tryAgain = view.findViewById(R.id.tryAgain);
        noInternetCategory = view.findViewById(R.id.noInternetCategory);



        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new CategoriesFragment());
            }
        });

        allDep = view.findViewById(R.id.allDep);
        allProd = view.findViewById(R.id.allProd);
        sliderpager = view.findViewById(R.id.slider_pager_department);
        indicator = view.findViewById(R.id.indicatorDepartment);

        mLayout = (RelativeLayout) view.findViewById(R.id.relative_categories);
        recDepartmentTab = (RecyclerView) view.findViewById(R.id.recDepartmentTab);
        recDepartmentTab.setHasFixedSize(true);
        layoutManagerDepartmentTab = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        categoriesTabAdapter = new CategoriesTabAdapter(getContext(), categoryTabs, CategoriesFragment.this);

        recDepartmentTab.setLayoutManager(layoutManagerDepartmentTab);
        categoryTabs = new ArrayList<>();

        recDepartment = (RecyclerView) view.findViewById(R.id.recDepartment);
        recDepartment.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getContext(), 3);
        recDepartment.setLayoutManager(layoutManager);

        recProduct = (RecyclerView) view.findViewById(R.id.recMainProduct);
        recProduct.setHasFixedSize(true);
        layoutManagerPro = new GridLayoutManager(getContext(), 3);
        recProduct.setLayoutManager(layoutManagerPro);

        mPresenter = new CategoriesPresenter(this);
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.categories_progress_bar);
        circularProgressBar.setVisibility(View.GONE);


        displayProductAdapter();

        visibleRecycleTap();

        MainActivity.filter = false;


        if (language == 1) {
            setLanguage(getActivity(), "ar");

            allDep.setText("جميع الأقسام");
            allProd.setText("جميع المنتجات");
            tryAgain.setText("اعادة المحاولة");
            noInternetCategory.setText("لايوجد اتصال بلشبكة يرجى المحاولة");
        } else if (language == 0) {
            setLanguage(getActivity(), "en");
            allDep.setText("All the Depatments");
            allProd.setText("All the Products");
            tryAgain.setText("Try again");
            noInternetCategory.setText("there isn't connection with internet");
        }

        return view;
    }

    public void displayCategoriesAdapter() {

        categoriesAdapter = new CategoriesAdapter(getContext(), mCategories);
        recDepartment.setAdapter(categoriesAdapter);
        categoriesAdapter.setData(mCategories);
        categoriesAdapter.notifyDataSetChanged();
    }


    public void displayProductAdapter() {

        productAdapter = new MainProductAdapter(getContext(), mProducts, CategoriesFragment.this);
        recProduct.setAdapter(productAdapter);
        productAdapter.notifyDataSetChanged();

    }

    public void displaySlideAdapter() {

        SliderPagerAdapter adapter = new SliderPagerAdapter(getContext(), mSlides);

        sliderpager.setAdapter(adapter);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                updateSlide.postAtFrontOfQueue(new Runnable() {

                    @Override
                    public void run() {

                        if (sliderpager.getCurrentItem() < mSlides.size() - 1) {

                            sliderpager.setCurrentItem(sliderpager.getCurrentItem() + 1);

                        } else {

                            sliderpager.setCurrentItem(0);
                        }
                    }

                });
            }
        }, delay, wait);

        indicator.setupWithViewPager(sliderpager, true);


    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragManager = myContext.getSupportFragmentManager();

        fragManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onShowLoading() {
        allDep.setVisibility(View.GONE);
        allProd.setVisibility(View.GONE);
        sliderpager.setVisibility(View.GONE);
//        mLayout.setBackgroundResource(R.color.green_opacity);
        circularProgressBar.setVisibility(View.VISIBLE);
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
//                , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onHideLoading() {
        try {
//            mLayout.setBackgroundResource(R.color.colorWhite);
            circularProgressBar.setVisibility(View.GONE);
//            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (Exception e) {

        }

    }

    @Override
    public void onGetCategoryOfferResult(List<Offers> offers) {
        if (offers != null) {
            mSlides = offers;
            if (mSlides.size() == 0) {
                sliderpager.setVisibility(View.GONE);
                indicator.setVisibility(View.GONE);
            } else {
                sliderpager.setVisibility(View.VISIBLE);
                //   indicator.setVisibility(View.VISIBLE);
                btnArrowLeft.setVisibility(View.VISIBLE);
                btnArrowRight.setVisibility(View.VISIBLE);
                displaySlideAdapter();
            }
        }
    }

    @Override
    public void onGetCategoryOfferError(String message) {

        GoneBecauseErrorConnection();
    }

    @Override
    public void onGetCategoryProductResult(ProductORCategory productORCategories) {
        mCategories.clear();
        categoriesAdapter.notifyDataSetChanged();
        mProducts.clear();

        if (productORCategories.getData_category().size() > 0) {
            allDep.setVisibility(View.VISIBLE);
            recDepartment.setVisibility(View.VISIBLE);
            mCategories = productORCategories.getData_category();

            onHideLoading();
        } else
            onHideLoading();


        if (productORCategories.getData_product().size() > 0) {
            Log.i("TAG", "onGetCategoryProductResult: " + productORCategories.getData_product().size());
            allProd.setVisibility(View.VISIBLE);
            recProduct.setVisibility(View.VISIBLE);
            mProducts = productORCategories.getData_product();
            displayProductAdapter();
            onHideLoading();

        } else
            onHideLoading();

        if (productORCategories.getData_product().size() == 0 && productORCategories.getData_category().size() == 0) {
            GoneBecauseNoData();
        }

    }


    @Override
    public void onGetCategoryProductError(String message) {
        GoneBecauseErrorConnection();
    }

    @Override
    public void onGetAllCategoriesResult(List<Categories> categories) {
        mCategories.clear();

        if (categories != null && categories.size() > 0) {
            allDep.setVisibility(View.VISIBLE);
            recDepartment.setVisibility(View.VISIBLE);
            mCategories = categories;
            displayCategoriesAdapter();


        }
    }

    @Override
    public void onGetAllCategoriesError(String message) {
        GoneBecauseErrorConnection();
    }

    @Override
    public void onGetAllOffersResult(List<Offers> offers) {
        if (offers != null) {
            mSlides = offers;
            sliderpager.setVisibility(View.VISIBLE);
            btnArrowRight.setVisibility(View.VISIBLE);
            btnArrowLeft.setVisibility(View.VISIBLE);
            displaySlideAdapter();
        }
    }

    @Override
    public void onGetAllOffersError(String message) {
        GoneBecauseErrorConnection();
    }

    @Override
    public void onGetCategoryEssentialResult(List<Categories> categories) {
        if (categoryTabs.size() == 0) {
            for (Categories categories1 : categories) {

                if (language == 1) {
                    CategoryTab categoryTab = new CategoryTab(categories1.name_ar, categories1.id);
                    categoryTabs.add(categoryTab);

                } else if (language == 0) {
                    CategoryTab categoryTab = new CategoryTab(categories1.name_en, categories1.id);
                    categoryTabs.add(categoryTab);

                }

            }
            try {
                categoryTabs.add(0, new CategoryTab(getString(R.string.main), 0));
                categoriesTabAdapter.setData(categoryTabs);
                recDepartmentTab.setAdapter(categoriesTabAdapter);
            } catch (Exception e) {

            }
        }

    }

    @Override
    public void onGetCategoryEssentialError(String message) {
        GoneBecauseErrorConnection();
    }

    public static void goneRecycleTap() {
        recDepartmentTab.setVisibility(View.GONE);
    }

    public static void visibleRecycleTap() {
        recDepartmentTab.setVisibility(View.VISIBLE);
    }

    public void MainTabPressed() {

        mCategories.clear();
        mProducts.clear();

        noItems.setVisibility(View.GONE);

        categoriesAdapter.setData(mCategories);

        mPresenter.getAllCategories();
        mPresenter.getAllOffers();

        recDepartment.setVisibility(View.GONE);
        recProduct.setVisibility(View.GONE);
        sliderpager.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
    }

    public void OtherTabPressed(int id) {

        mCategories.clear();
        mProducts.clear();

        noItems.setVisibility(View.GONE);

        categoriesAdapter = new CategoriesAdapter(getContext(), mCategories);
        categoriesAdapter.setData(mCategories);

        recDepartment.setVisibility(View.GONE);
        recProduct.setVisibility(View.GONE);
        sliderpager.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);

        mPresenter.getCategoryOffer(id);
        mPresenter.getCategoryProduct(id);
    }

    @Override
    public void onStart() {
        super.onStart();


        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        allDep.setVisibility(View.GONE);
        allProd.setVisibility(View.GONE);

        if (networkInfo != null && networkInfo.isConnected()) {
            if (CategoriesFragment.pos == 0) {
                mPresenter.getAllCategories();

                mPresenter.getAllOffers();

                mPresenter.getAllDepartment();


            } else {
                mPresenter.getAllDepartment();

                allDep.setVisibility(View.GONE);
                allProd.setVisibility(View.GONE);
            }
            recDepartmentTab.setVisibility(View.VISIBLE);
            noWifi.setVisibility(View.GONE);
        } else {
            recDepartmentTab.setVisibility(View.GONE);
            noWifi.setVisibility(View.VISIBLE);
        }
    }

    public void GoneBecauseErrorConnection() {
        recDepartment.setVisibility(View.GONE);
        recProduct.setVisibility(View.GONE);
        sliderpager.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
        recDepartmentTab.setVisibility(View.GONE);
        allProd.setVisibility(View.GONE);
        allDep.setVisibility(View.GONE);

        noWifi.setVisibility(View.VISIBLE);
    }

    public void GoneBecauseNoData() {
        recDepartment.setVisibility(View.GONE);
        recProduct.setVisibility(View.GONE);
        sliderpager.setVisibility(View.GONE);
        btnArrowLeft.setVisibility(View.GONE);
        btnArrowRight.setVisibility(View.GONE);
        allProd.setVisibility(View.GONE);
        allDep.setVisibility(View.GONE);

        noItems.setVisibility(View.VISIBLE);
    }

    public void setLanguage(Context c, String lang) {
        Locale localeNew = new Locale(lang);
        Locale.setDefault(localeNew);

        Resources res = c.getResources();
        Configuration newConfig = new Configuration(res.getConfiguration());
        newConfig.locale = localeNew;
        newConfig.setLayoutDirection(localeNew);
        res.updateConfiguration(newConfig, res.getDisplayMetrics());

        newConfig.setLocale(localeNew);
        c.createConfigurationContext(newConfig);
    }
}
